import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/login';
import Consultation from '@/components/consultation';
import Create from '@/components/create';
import Header from '@/components/header';
import Sortie from '@/components/sortie';
import Mdp_oublie from '@/components/mdp_oublie';

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/consultation',
      name: 'Consultation',
      component: Consultation
    },
      
     {
       path: '/sortie',
       name: 'Sortie',
       component: Sortie
     },
     {
       path: '/mdp_oublie',
       name: 'Mdp_oublie',
       component: Mdp_oublie
     },
    {
      path: '/create',
      name: 'Create',
      component:Create
    },
    
    {
      path: '/header',
      name: 'Header',
      component:Header
    },
    {
      path: '*',
      redirect: 'Header',
      component:Header
    }
    
  ]
})